﻿using System;
using System.Collections.Generic;
using System.IO;

namespace JavaExercise
{
    class Program
    {
        static bool validated = false;
        static string[] commandWords;

        static void Main(string[] args)
        {
            Console.WriteLine("*********Directory listing*********");
            Console.WriteLine("Please insert your command: ");
            do
            {
                
                string command = Console.ReadLine().ToLower();
                commandWords = command.Split(' ');
                validated = ValidateCommand();
                
            } while (!validated);

            ListSubDirectories();
            Console.ReadLine();
            
        }

        private static bool ValidateCommand()
        {


            if (commandWords[0] == "java")
            {

                if (commandWords.Length >= 2 && commandTypes.ContainsValue(commandWords[1]))
                {
                    if (commandWords.Length > 2 )
                    {
                        ListSubDirectories();
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("The path can not be empy");
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("Please select a valid command from the following list:");
                    foreach (KeyValuePair<int, string> entry in commandTypes)
                    {
                        Console.WriteLine(entry.Value);
                    }
                    return false;
                }
            }

            else
            {
                Console.WriteLine("Command not recognized, please insert a valid command: ");
                return false;
            }
            
        }

        private static void ListSubDirectories()
        {
            try
            {
                string[] allfiles = Directory.GetFiles(commandWords[2], "*", SearchOption.TopDirectoryOnly);
                if (allfiles.Length > 0)
                {
                    foreach (var item in allfiles)
                    {
                        Console.WriteLine(item);
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                throw;
            }
           
        }

        public static Dictionary<int, string> commandTypes = new Dictionary<int, string>()
            {
                { 1, "du"  }
            };


        
        }

       
    }

    

